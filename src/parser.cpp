#include "parser.hpp"
#include <functional>
#include <iostream>
#include <unordered_map>

std::unique_ptr<AST> Parser::parse(std::istream& iss) {
  json j;
  iss >> j;

  return parse(j);
}

using Key = std::string;

Key IntegerKey = "Integer";
Key BooleanKey = "Boolean";
Key NullKey = "Integer";
Key VariableKey = "Variable";
Key AccessVariableKey = "AccessVariable";
Key AssignVariableKey = "AssignVariable";
Key ArrayKey = "Array";
Key AccessArrayKey = "AccessArray";
Key AssignArrayKey = "AssignArray";
Key FunctionKey = "Function";
Key CallFunctionKey = "CallFunction";
Key PrintKey = "Print";
Key BlockKey = "Block";
Key TopKey = "Top";
Key LoopKey = "Loop";
Key ConditionalKey = "Conditional";

std::unique_ptr<AST> Parser::parse(json& j) {
  for (auto& f : j.items()) {
    auto& key = f.key();
    if (IntegerKey == key) {
      auto i = std::make_unique<Integer>();
      i->value = f.value();
      return i;
    } else if (BooleanKey == key) {
      auto i = std::make_unique<Boolean>();
      i->value = f.value();
      return i;
    } else if (NullKey == key) {
      return nullptr;
    } else if (VariableKey == key) {
      auto i = std::make_unique<VarDef>();
      i->name = f.value()["name"];
      i->value = parse(f.value()["value"]);
      return i;
    } else if (AccessVariableKey == key) {
      auto i = std::make_unique<VarAccess>();
      i->name = f.value()["name"];
      return i;
    } else if (AssignVariableKey == key) {
      auto i = std::make_unique<VarAssign>();
      i->name = f.value()["name"];
      i->value = parse(f.value()["value"]);
      return i;
    } else if (ArrayKey == key) {
      auto i = std::make_unique<ArrayDef>();
      i->size = parse(f.value()["size"]);
      i->value = parse(f.value()["value"]);
      return i;
    } else if (AccessArrayKey == key) {
      auto i = std::make_unique<ArrayAccess>();
      i->array = parse(f.value()["array"]);
      i->index = parse(f.value()["index"]);
      return i;
    } else if (AssignArrayKey == key) {
      auto i = std::make_unique<ArrayAssign>();
      i->array = parse(f.value()["array"]);
      i->index = parse(f.value()["index"]);
      i->value = parse(f.value()["value"]);
      return i;
    } else if (FunctionKey == key) {
      auto i = std::make_unique<Function>();
      i->name = f.value()["name"];
      i->body = parse(f.value()["body"]);
      i->parameters = f.value()["parameters"].get<std::vector<Identifier>>();
      return i;
    } else if (CallFunctionKey == key) {
      auto i = std::make_unique<CallFunction>();
      i->name = f.value()["name"];
      for (auto& k : f.value())
        i->arguments.push_back(parse(k));
      return i;
    } else if (PrintKey == key) {
      auto i = std::make_unique<PrintCall>();
      i->format = f.value()["format"];
      for (auto& k : f.value())
        i->arguments.push_back(parse(k));
      return i;
    } else if (BlockKey == key) {
      auto i = std::make_unique<Block>();
      for (auto& k : f.value())
        i->expressions.push_back(parse(k));
      return i;
    } else if (TopKey == key) {
      auto i = std::make_unique<Top>();
      for (auto& k : f.value())
        i->statements.push_back(parse(k));
      return i;
    } else if (LoopKey == key) {
      auto i = std::make_unique<Loop>();
      i->body = parse(f.value()["body"]);
      i->condition = parse(f.value()["condition"]);
      return i;
    } else if (ConditionalKey == key) {
      auto i = std::make_unique<Conditional>();
      i->condition = parse(f.value()["condition"]);
      i->consequent = parse(f.value()["consequent"]);
      i->alternative = parse(f.value()["alternative"]);
      return i;
    }
    std::cout << f << std::endl;
  }
  return nullptr;
}