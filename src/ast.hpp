#include <algorithm>
#include <string>
#include <vector>

using Identifier = std::string;

class AbstractVisitor;

class AST {
  virtual void accept(const AbstractVisitor*) const = 0;
};

class Integer;
class Boolean;
class Null;
class VarDef;
class VarAccess;
class VarAssign;
class ArrayDef;
class ArrayAccess;
class ArrayAssign;
class Function;
class CallFunction;
class PrintCall;
class Block;
class Top;
class Loop;
class Conditional;

class AbstractVisitor {
 public:
  virtual void visit(const Integer*) const = 0;
  virtual void visit(const Boolean*) const = 0;
  virtual void visit(const Null*) const = 0;
  virtual void visit(const VarDef*) const = 0;
  virtual void visit(const VarAccess*) const = 0;
  virtual void visit(const VarAssign*) const = 0;
  virtual void visit(const ArrayDef*) const = 0;
  virtual void visit(const ArrayAccess*) const = 0;
  virtual void visit(const ArrayAssign*) const = 0;
  virtual void visit(const Function*) const = 0;
  virtual void visit(const CallFunction*) const = 0;
  virtual void visit(const PrintCall*) const = 0;
  virtual void visit(const Block*) const = 0;
  virtual void visit(const Top*) const = 0;
  virtual void visit(const Loop*) const = 0;
  virtual void visit(const Conditional*) const = 0;
};

class Integer : public AST {
 public:
  int value;

  virtual void accept(const AbstractVisitor* v) const { v->visit(this); }
};

class Boolean : public AST {
 public:
  bool value;
  virtual void accept(const AbstractVisitor* v) const { v->visit(this); }
};

class Null : public AST {
 public:
  virtual void accept(const AbstractVisitor* v) const { v->visit(this); }
};

class VarDef : public AST {
 public:
  Identifier name;
  std::unique_ptr<AST> value;
  virtual void accept(const AbstractVisitor* v) const { v->visit(this); }
};

class VarAccess : public AST {
 public:
  Identifier name;
  virtual void accept(const AbstractVisitor* v) const { v->visit(this); }
};

class VarAssign : public AST {
 public:
  Identifier name;
  std::unique_ptr<AST> value;
  virtual void accept(const AbstractVisitor* v) const { v->visit(this); }
};

class ArrayDef : public AST {
 public:
  std::unique_ptr<AST> size;
  std::unique_ptr<AST> value;
  virtual void accept(const AbstractVisitor* v) const { v->visit(this); }
};

class ArrayAccess : public AST {
 public:
  std::unique_ptr<AST> array;
  std::unique_ptr<AST> index;
  virtual void accept(const AbstractVisitor* v) const { v->visit(this); }
};

class ArrayAssign : public AST {
 public:
  std::unique_ptr<AST> array;
  std::unique_ptr<AST> index;
  std::unique_ptr<AST> value;
  virtual void accept(const AbstractVisitor* v) const { v->visit(this); }
};

class Function : public AST {
 public:
  Identifier name;
  std::vector<Identifier> parameters;
  std::unique_ptr<AST> body;
  virtual void accept(const AbstractVisitor* v) const { v->visit(this); }
};

class CallFunction : public AST {
 public:
  Identifier name;
  std::vector<std::unique_ptr<AST>> arguments;
  virtual void accept(const AbstractVisitor* v) const { v->visit(this); }
};

class PrintCall : public AST {
 public:
  std::string format;
  std::vector<std::unique_ptr<AST>> arguments;
  virtual void accept(const AbstractVisitor* v) const { v->visit(this); }
};

class Block : public AST {
 public:
  std::vector<std::unique_ptr<AST>> expressions;
  virtual void accept(const AbstractVisitor* v) const { v->visit(this); }
};

class Top : public AST {
 public:
  std::vector<std::unique_ptr<AST>> statements;
  virtual void accept(const AbstractVisitor* v) const { v->visit(this); }
};

class Loop : public AST {
 public:
  std::unique_ptr<AST> condition;
  std::unique_ptr<AST> body;
  virtual void accept(const AbstractVisitor* v) const { v->visit(this); }
};

class Conditional : public AST {
 public:
  std::unique_ptr<AST> condition;
  std::unique_ptr<AST> consequent;
  std::unique_ptr<AST> alternative;
  virtual void accept(const AbstractVisitor* v) const { v->visit(this); }
};