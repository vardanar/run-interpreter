#include <istream>
#include <nlohmann/json.hpp>
#include "ast.hpp"

using namespace nlohmann;

class Parser {
 public:
  std::unique_ptr<AST> parse(std::istream&);

 private:
  std::unique_ptr<AST> parse(json&);
};