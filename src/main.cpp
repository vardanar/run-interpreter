#include <iostream>
#include <nlohmann/json.hpp>
#include <string>
#include "parser.hpp"

using namespace nlohmann;

int main(void) {
  std::unique_ptr<AST> ast = Parser().parse(std::cin);
}