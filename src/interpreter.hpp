#include <ast.hpp>

class Interpreter {
 public:
  int interpret(AST*);
};
